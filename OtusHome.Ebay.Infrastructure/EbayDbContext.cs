﻿using Microsoft.EntityFrameworkCore;
using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;

namespace OtusHome.Ebay.Infrastructure
{
    public class EbayDbContext : DbContext
    {
        public EbayDbContext(DbContextOptions options):base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderRow> OrderRows { get; set; }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().HasData(
                new User()
                {
                    Id = 1,
                    Login = "User1"
                },
                new User()
                {
                    Id = 2,
                    Login = "User2"
                });

            builder.Entity<Category>().HasData(
                new Category
                {
                    Id = 1,
                    Name = "Телевизоры",
                },
                new Category
                {
                    Id = 2,
                    Name = "Холодильники"
                }
            );

            builder.Entity<Product>().HasData(
                new List<Product>()
                {
                    new Product()
                    {
                        Id = 1,
                        Name = "Samsung 150",
                        Price = 17000,
                        Count = 11,
                        CategoryId = 1
                    },
                    new Product()
                    {
                        Id = 2,
                        Name = "Philips 72",
                        Price = 15000,
                        Count = 5,
                        CategoryId = 1
                    },
                    new Product()
                    {
                        Id = 3,
                        Name = "Polar 2",
                        Price = 25000,
                        Count = 3,
                        CategoryId = 2
                    },
                    new Product()
                    {
                        Id = 4,
                        Name = "Indezit M3",
                        Price = 21000,
                        Count = 2,
                        CategoryId = 2
                    }
                });

            builder.Entity<Order>()
                .HasData(
                    new Order()
                    {
                        Id = 1,
                        Date = DateTime.Today,
                        UserID = 1
                    });
            builder.Entity<OrderRow>().HasData(
                new OrderRow()
                {
                    Id = 1,
                    Count = 2,
                    OrderId = 1,
                    ProductId = 1,
                    Discount = 10,
                    SellingPrice = 17000
                },
                new OrderRow()
                {
                    Id = 2,
                    Count = 1,
                    OrderId = 1,
                    ProductId = 2,
                    Discount = 10,
                    SellingPrice = 13000
                });
        }
    }
}
