﻿using Microsoft.EntityFrameworkCore;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Infrastructure.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly EbayDbContext _db;

        public OrderRepository(EbayDbContext db)
        {
            _db = db;
        }

        public async ValueTask<Order> Add(Order entity)
        {
            _db.Orders.Add(entity);

            await _db.SaveChangesAsync();

            return entity;
        }

        public ValueTask<Order> GetAsync(int id)
        {
            return _db.Orders.FindAsync(id);
        }
        
        public async Task<IEnumerable<Order>> GetByUserId(int userId)
        {
            return await _db.Orders.Where(x => x.User.Id == userId).ToListAsync();
        }

        public void Remove(Order entity)
        {
            _db.Orders.Remove(entity);
            _db.SaveChangesAsync();
        }

        public void RemoveRange(IEnumerable<Order> entities)
        {
            _db.Orders.RemoveRange(entities);
            _db.SaveChangesAsync();
        }

        public void Update(Order entity)
        {
            _db.Orders.Update(entity);
            _db.SaveChangesAsync();
        }
    }
}
