﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly EbayDbContext _db;

        public ProductRepository(EbayDbContext db)
        {
            _db = db;
        }

        public Task<IEnumerable<Product>> GetProductsByCategory(int categoryId, int skip, int take)
        {
            var dbConnection = _db.Database.GetDbConnection();

            const string sql = @"SELECT * FROM ""Products"" p WHERE ""CategoryId"" = @CategoryId LIMIT @Take OFFSET @Skip";
            return dbConnection.QueryAsync<Product>(sql, new { CategoryId = categoryId, Take = take, Skip = skip});
        }

        public Task<Product> GetByName(string name)
        {
            return _db.Products.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        public ValueTask<Product> GetAsync(int id)
        {
            return _db.FindAsync<Product>(id);
        }

        public async ValueTask<Product> Add(Product entity)
        {
            _db.Products.Add(entity);

            await _db.SaveChangesAsync();

            return entity;
        }

        public void Update(Product entity)
        {
            _db.Products.Update(entity);

            _db.SaveChangesAsync();
        }

        public void Remove(Product entity)
        {
            _db.Products.Remove(entity);

            _db.SaveChangesAsync();
        }

        public void RemoveRange(IEnumerable<Product> entities)
        {
            _db.Products.RemoveRange(entities);

            _db.SaveChangesAsync();
        }
    }
}
