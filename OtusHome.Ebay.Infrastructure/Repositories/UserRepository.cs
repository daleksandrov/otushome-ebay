﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;
using StackExchange.Redis;

namespace OtusHome.Ebay.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly EbayDbContext _db;
        private readonly ILogger _logger;

        private readonly IDatabase _redis;

        public UserRepository(EbayDbContext db, ILogger<User> logger, IDatabase redis) 
        {
            _db = db;
            _logger = logger;
            _redis = redis;
        }

        public async ValueTask<User> GetAsync(int id)
        {
            var fromCache = await GetUserFromCache(id);
            if (fromCache != null)
            {
                return fromCache;
            }

            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == id);

            _ = AddUserToCache(user);

            return user;
        }

        public async Task<User> GetByLogin(string login)
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Login == login);
            
            return user;
        }

        public async ValueTask<User> Add(User user)
        {
            _db.Users.Add(user);

            await _db.SaveChangesAsync();

            return user;
        }

        public void Update(User entity)
        {
            _db.Users.Update(entity);

            _db.SaveChangesAsync();
        }

        public void Remove(User entity)
        {
            _db.Users.Remove(entity);

            _db.SaveChangesAsync();
        }

        public void RemoveRange(IEnumerable<User> entities)
        {
            _db.Users.RemoveRange(entities);

            _db.SaveChangesAsync();
        }

        private async Task<User> GetUserFromCache(int id)
        {
            var redisKey = GetCacheKey(id);
            string cache = await _redis.StringGetAsync(redisKey);

            if (!string.IsNullOrEmpty(cache))
            {
                try
                {
                    return JsonSerializer.Deserialize<User>(cache);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Could not deserialize user");
                }
            }

            return null;
        }

        private async Task AddUserToCache(User user)
        {
            if (user != null)
            {
                var redisKey = GetCacheKey(user.Id);
                await _redis.StringSetAsync(redisKey, JsonSerializer.Serialize(user), flags: CommandFlags.FireAndForget);
            }
        }

        private static string GetCacheKey(int id)
        {
            return $"users:id_{id}";
        }
    }
}
