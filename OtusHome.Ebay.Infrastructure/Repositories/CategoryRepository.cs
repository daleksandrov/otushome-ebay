﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;

namespace OtusHome.Ebay.Infrastructure.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly EbayDbContext _db;
        public CategoryRepository(EbayDbContext dbContext)
        {
            _db = dbContext;
        }

        public async ValueTask<Category> Add(Category entity)
        {
            _db.Categories.Add(entity);

            await _db.SaveChangesAsync();

            return entity;
        }
        
        public ValueTask<Category> GetAsync(int id)
        {
            return _db.Categories.FindAsync(id);
        }

        public Task<IEnumerable<Category>> GetCategories(int skip, int take)
        {
            var dbConnection = _db.Database.GetDbConnection();

            const string sql = "SELECT * FROM Categories C LIMIT @Take OFFSET @Skip";
            return dbConnection.QueryAsync<Category>(sql, new { Take = take, Skip = skip });
        }

        public void Remove(Category entity)
        {
            _db.Categories.Remove(entity);
            _db.SaveChangesAsync();
        }

        public void RemoveRange(IEnumerable<Category> entities)
        {
            _db.Categories.RemoveRange(entities);
            _db.SaveChangesAsync();
        }

        public void Update(Category entity)
        {
            _db.Categories.Update(entity);
            _db.SaveChangesAsync();
        }
    }
}
