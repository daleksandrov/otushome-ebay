﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtusHome.Ebay.Infrastructure
{
    internal class EbayDbContextDesignTimeFactory : IDesignTimeDbContextFactory<EbayDbContext>
    {
        public EbayDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<EbayDbContext>();
            optionsBuilder.UseSqlite("Filename=Ebay.db");

            return new EbayDbContext(optionsBuilder.Options);
        }
    }
}
