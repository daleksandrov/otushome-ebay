﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OtusHome.Ebay.Domain
{
    public class Product
    {
        public int Id { get; set; }

        public string Name {get; set; }

        public decimal Price { get; set; }

        public int Count { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
