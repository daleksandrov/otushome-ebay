﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OtusHome.Ebay.Domain
{
    public class OrderRow
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int Count { get; set; }

        public decimal SellingPrice { get; set; }

        public decimal Discount { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
