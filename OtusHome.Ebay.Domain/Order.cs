﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OtusHome.Ebay.Domain
{
    public class Order
    {
        public int Id { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }

        public DateTime Date { get; set; }        

        public List<OrderRow> OrderRows { get; set; }
    }
}
