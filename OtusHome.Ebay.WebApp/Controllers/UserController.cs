﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;

namespace OtusHome.Ebay.WebApi.Controllers
{
    /// <summary>
    /// API пользователей
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        public UserController(IUserRepository userRepository, IUserService userService)
        {
            _userRepository = userRepository;
            _userService = userService;
        }

        [HttpGet("{id:int}")]
        public async ValueTask<ActionResult<User>> Get(int id)
        {
            var user = await _userRepository.GetAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public async ValueTask<ActionResult<User>> Get([FromBody] User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            await _userService.AddNewUser(user);

            return Ok(user);
        }
    }
}
