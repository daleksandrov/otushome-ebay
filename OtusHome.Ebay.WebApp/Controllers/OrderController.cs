﻿using Microsoft.AspNetCore.Mvc;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OtusHome.Ebay.WebApi.Controllers
{
    public class OrderController: ControllerBase
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            this._orderRepository = orderRepository;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Order>> Get(int id)
        {
            var order = await _orderRepository.GetAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        [HttpGet("list")]
        public async Task<ActionResult> GetList(int userId)
        {
            var orders = await _orderRepository.GetByUserId(userId);
            if (orders.Any())
            {
                return NotFound();
            }

            return Ok(orders);
        }

    }
}
