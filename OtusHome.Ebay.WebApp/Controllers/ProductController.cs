﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OtusHome.Ebay.Application;
using OtusHome.Ebay.Domain;

namespace OtusHome.Ebay.WebApi.Controllers
{
    /// <summary>
    /// API товаров
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet("{id:int}")]
        public async ValueTask<ActionResult<Product>> Get(int id)
        {
            var product = await _productRepository.GetAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpGet("list")]
        public async Task<ActionResult> Get(int categoryId, int take, int skip = 0)
        {
            var products = await _productRepository.GetProductsByCategory(categoryId, skip, take);
            if (products.Any())
            {
                return NotFound();
            }

            return Ok(products);
        }

        [HttpGet("Name")]
        public async Task<ActionResult<Product>> Get(string name)
        {
            var product = await _productRepository.GetByName(name);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPost]
        public async Task<ActionResult<Product>> Post([FromBody] Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            await _productRepository.Add(product);

            return Ok(product);
        }
    }
}