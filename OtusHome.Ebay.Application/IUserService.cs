﻿using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Application
{
    public interface IUserService
    {
        Task<User> AddNewUser(User user);
    }
}
