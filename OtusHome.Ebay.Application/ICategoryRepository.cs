﻿using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Application
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<IEnumerable<Category>> GetCategories(int skip, int take);
    }
}
