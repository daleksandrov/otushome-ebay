﻿using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Application
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> AddNewUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var loginIsTaken = await _userRepository
              .GetByLogin(user.Login)
              .ContinueWith(x => x.Result != null);

            if (loginIsTaken)
            {
                throw new InvalidOperationException("Пользователь с таким логином уже существует");
            }

            return await _userRepository.Add(user);
        }
    }
}
