﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Application
{
    public interface IRepository<TEntity>
    {
        ValueTask<TEntity> GetAsync(int id);
        ValueTask<TEntity> Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
