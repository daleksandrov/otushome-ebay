﻿using OtusHome.Ebay.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OtusHome.Ebay.Application
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IEnumerable<Product>> GetProductsByCategory(int categoryId, int skip, int take);

        Task<Product> GetByName(string name);
    }
}
